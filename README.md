# README #

The files feat5_1.csv and labels5_1.csv constitute the dataset generated from 5 random simulations in SUMO that represents a set of 5s trajectories for vehicles in the simulations.

Each row in feat5_1.csv is a vector of features where each vector is of the form:
(x4, y4, ..., x3, y3, ..., x0, y0, v0, d2i0, d2v0) where
x, y --> position of the vehicle
v --> speed
d2i --> distance to the nearest intersection
d2v --> distance to the leading vehicle

Each row in labels_1.csv represents the pair of output labels for the corresponding row in feat5_1.csv where the first element of the pair represents the turn label and the second element represents the acceleration label where:
turn label can be any of 0 (None), 1 (RT), or 2 (LT) 
acceleration label can be any of 0 (CV), 1 (Accelerate) or 2 (Decelerate)